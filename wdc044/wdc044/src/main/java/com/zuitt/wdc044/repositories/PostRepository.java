package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//an interface contains behavior that a class implements
//an interface marked as @Repository contains methods for database manipulation
//by extending CrudRepository, PostRepository has inherited its pre-defined methods for creating, updating, and deleting
public interface PostRepository extends CrudRepository<Post, Object> {
    Iterable<Post> findByUser_Id(Long userId);
}
