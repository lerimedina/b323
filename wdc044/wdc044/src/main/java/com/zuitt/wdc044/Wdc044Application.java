package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
//This indicate that a class is a controller that will handle restful web request and returns an HTTP response
@RestController
public class Wdc044Application {
//This method starts the whole spring framework.
//	This serves as the entry point to start the application
	public static void main(String[] args) {

		SpringApplication.run(Wdc044Application.class, args);
	}
//	This is used to map HTTP GET requests
	// Note: HTTP Request annotations is usually followed by a method body
	//The method body contains the logic to generate the response
	@GetMapping	("/hello")
	// @RequestParam is used to extract query parameters, form parameters, and even files from the request.
//	if the URL is :/hello?name=john the method will return "Hello john"
//	? means the that the start of the parameter followed by the key = value pair
//	if the URL is :/hello, the method will return "Hello World"
	public String hello(@RequestParam(value="name", defaultValue = "World") String name){
//		To send a response of "Hello + name"
		return String.format("Hello %s!", name);

	}
	@GetMapping("/hi")
	public String hi(@RequestParam(value="name", defaultValue = "user") String name){
		return String.format("hi %s!", name);
	}

}

